var base_url = "http://localhost/CI/gated/";

//send password recovery emails

(function send_password_emails() {
    $.ajax({
        type: "POST",
        url: base_url + "auth/send_password_emails",
        success: function () {

        },
        complete: function () {
            setTimeout(send_password_emails, 1000);
        }
    });
})();

//login form

$(document).ready(function () {
    $("#loginform").submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serializeArray(),
            beforeSend: function () {
                $("#loader_login").show();
                $("#login_text_default").hide();
                $("#login_text").show();
                $("#login_text").text("Checking credentials...  ");
            },

            success: function (res) {
                if(res === '1')
                {
                    $("#loader_login").hide();
                    $("#login_text_default").show();
                    $("#login_text").hide();
                    $("#wrong_details").show();

                    return false;
                }
                else
                {
                    $("#loader_login").hide();
                    $("#login_text_default").hide();
                    $("#login_text").show();
                    $("#login_text").text("Correct...  Redirecting...");

                    setTimeout(function () {
                        window.location.href = base_url + "auth/login_ajax/" + res;
                    }, 1000);
                }
            }

        });
    });
});

//registration form

$(document).ready(function () {
    $("#registration_form").submit(function (e) {
        e.preventDefault();

        var pass1 = document.getElementById('password1').value;
        var pass2 = document.getElementById('password2').value;

        if(pass1 !== pass2)
        {
            $("#password2").css({"border-bottom": "solid 1px red"});
            $("#incorrect_password").show();
        }
        else
        {
            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: $(this).serializeArray(),
                beforeSend: function () {
                    $("#submit-btn").addClass("disabled");
                    $("#loader").show();
                    $("#button_text_default").hide();
                    $("#button_text").show();
                    $("#button_text").text("Setting up account...  ");
                },
                success: function (res) {
                    if(res === '1')
                    {
                        $("#submit-btn").removeClass("disabled");
                        $("#loader").hide();
                        $("#button_text_default").show();
                        $("#button_text").hide();
                        $("#email").css({"border-bottom": "solid 1px red"});
                        $("#incorrect_email").show();

                        return false;
                    }
                    else
                    {
                        $("#submit-btn").addClass("disabled");
                        $("#loader").hide();
                        $("#button_text_default").hide();
                        $("#button_text").show();
                        $("#button_text").text("Successful...  Redirecting...");

                        setTimeout(function () {
                            window.location.href = base_url + "auth/login_ajax/" + res;
                        }, 1000);
                    }
                }
            });
        }

    });
});

//email recovery

$(document).ready(function () {
    $("#recoverform").submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serializeArray(),
            beforeSend: function () {
                $("#loader_recover").show();
                $("#recover_text_default").hide();
                $("#recover_text").show();
                $("#recover_text").text("Checking Email Address...  ");
            },

            success: function (res) {
                if(res === '1')
                {
                    $("#loader_recover").hide();
                    $("#recover_text_default").show();
                    $("#recover_text").hide();
                    $("#wrong_email").show();

                    return false;
                }
                else
                {
                    $("#loader_recover").hide();
                    $("#recover_text_default").show();
                    $("#recover_text").hide();
                    $("#wrong_email").hide();
                    $("#correct_email").show();
                }
            }

        });
    });
});

// add a house to a community

$(document).ready(function () {
    $("#house_details_form").submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serializeArray(),
            beforeSend: function () {
                swal({
                    title: "Please Wait",
                    text: "Processing House Details...",
                    type: "info",
                    showConfirmButton: false,
                    showCancelButton: false
                });
            },

            success: function (res) {

                if(res)
                {
                    swal({
                        title: "Process Successful",
                        text: "Apartment Detail Added Successfully.",
                        type: "success",
                        showConfirmButton: true,
                        confirmButtonText: "Add Units To This Apartment",
                        confirmButtonColor: "#32CD32",
                        showCancelButton: false
                    },function () {
                        window.location.href = base_url + "court/property/" + res;
                    });
                }
                else
                {
                    swal({
                        title: "Process Successful",
                        text: "House Detail Added Successfully.",
                        type: "success",
                        showConfirmButton: true,
                        confirmButtonText: "Add Another Property",
                        confirmButtonColor: "#32CD32",
                        showCancelButton: false
                    },function () {
                        window.location.reload();
                    });
                }
            }
        });
    });
});

// add a units to an apartment

$(document).ready(function () {
    $("#unit_add_form").submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serializeArray(),
            beforeSend: function () {
                swal({
                    title: "Please Wait",
                    text: "Processing Unit Details...",
                    type: "info",
                    showConfirmButton: false,
                    showCancelButton: false
                });
            },

            success: function () {

                swal({
                    title: "Process Successful",
                    text: "Apartment Units Added Successfully.",
                    type: "success",
                    showConfirmButton: true,
                    confirmButtonText: "Back To Add Property",
                    confirmButtonColor: "#32CD32",
                    showCancelButton: false
                },function () {
                    window.location.href = base_url + "court/add_property/";
                });
            }

        });
    });
});

// add a units to an apartment (csv)

$(document).ready(function () {
    $("#unit_add_form_csv").submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: new FormData(this),
            processData: false,
            contentType: false,
            beforeSend: function () {
                swal({
                    title: "Please Wait",
                    text: "Processing Unit Details...",
                    type: "info",
                    showConfirmButton: false,
                    showCancelButton: false
                });
            },

            success: function (res) {

                if(res === "0")
                {
                    swal({
                        title: "Server Error",
                        text: "Unable to upload file. Try again or contact support.",
                        type: "warning",
                        showConfirmButton: true,
                        showCancelButton: false
                    },function () {
                        window.location.reload();
                    });
                }

                else if(res === "1")
                {
                    swal({
                        title: "Process Fail",
                        text: "Please upload a csv file",
                        type: "error",
                        showConfirmButton: false,
                        showCancelButton: true
                    });
                }

                else
                {
                    swal({
                        title: "Process Successful",
                        text: "Apartment Units Added Successfully.",
                        type: "success",
                        showConfirmButton: true,
                        confirmButtonText: "Add Another Property",
                        confirmButtonColor: "#32CD32",
                        showCancelButton: false
                    },function () {
                        window.location.href = base_url + "court/add_property";
                    });
                }
            }

        });
    });
});


