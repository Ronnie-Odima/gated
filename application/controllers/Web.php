<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller
{
    public function index()
    {
        $data = array
        (
            'title' => 'Home'
        );

        $this->load->view('web/temps/head', $data);
        $this->load->view('web/temps/header', $data);
        $this->load->view('web/temps/sidebar', $data);
        $this->load->view('web/pages/body', $data);
        $this->load->view('web/pages/footer', $data);
        $this->load->view('web/temps/scripts', $data);
    }

}