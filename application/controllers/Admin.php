<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function dashboard()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Home',
            's_id' => $session,
            'page' => $this->uri->segment(2)
        );

        $this->load->view('admin/temps/head', $data);
        $this->load->view('admin/temps/header', $data);
        $this->load->view('admin/temps/sidebar', $data);
        $this->load->view('admin/pages/dashboard', $data);
        $this->load->view('admin/pages/footer', $data);
        $this->load->view('admin/temps/scripts', $data);

    }

    public function add_community()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gated :: Add Community',
            's_id' => $session,
            'page' => $this->uri->segment(2)
        );

        $this->load->view('admin/temps/head', $data);
        $this->load->view('admin/temps/header', $data);
        $this->load->view('admin/temps/sidebar', $data);
        $this->load->view('admin/pages/add_community', $data);
        $this->load->view('admin/pages/footer', $data);
        $this->load->view('admin/temps/scripts', $data);
    }

    public function process_community_data()
    {
        $session = $this->check_session();

        $ci_d = uniqid();

        $basic_data = array(
            'c_id' => $ci_d,
            'name' => $this->input->post('c_name'),
            'city' => $this->input->post('city'),
            'location' => $this->input->post('location'),
            'website' => $this->input->post('website'),
            'description' => $this->input->post('description'),
            'p_fname' => $this->input->post('p_fname'),
            'p_lname' => $this->input->post('p_lname'),
            'p_email' => $this->input->post('p_email'),
            'p_phone' => $this->input->post('p_phone'),
            'image' => "",
            'added_by' => $session
        );

        $this->db->insert('community', $basic_data);

        if (!empty($_FILES['main_image']['name']))
        {
            $name = $_FILES['main_image']['name'];
            $tmp_name = $_FILES['main_image']['tmp_name'];
            $path = './assets/photos/';

            $uploaded_image = $path . $name;

            $data_image = array(
                'image' => $uploaded_image
            );

            $this->db->where('c_id', $ci_d);
            $result = $this->db->update('community', $data_image);

            if ($result == true) {
                move_uploaded_file($tmp_name, $path . $name);
            }
        }

        $amenities = $this->input->post('amenities');

        foreach ($amenities as $amenity)
        {
            $data_amenity = array(
                'c_id' => $ci_d,
                'amenity' => $amenity
            );

            $this->db->insert('amenities', $data_amenity);
        }

        if (!empty($_FILES['other_images']['name'][0]))
        {
            $files = $_FILES['other_images'];
            $uploaded = array();

            //loop
            foreach ($files['name'] as $position=>$file_name)
            {
                $tmp_name = $files['tmp_name'][$position];
                $file_size = $files['size'][$position];
                $file_error = $files['error'][$position];
                $file_ext = explode('.',$file_name);
                $file_ext = strtolower(end($file_ext));

                if ($file_error === 0)
                {
                    if ($file_size <= 209791559)
                    {
                        $file_name_new = uniqid('',true).".".$file_ext;
                        $file_destination = './assets/photos/'.$file_name_new;
                        if (move_uploaded_file($tmp_name,$file_destination))
                        {
                            $uploaded[$position] = $file_destination;
                        }

                    }
                }
            }
            //end loop

            if(!empty($uploaded))
            {
                foreach ($uploaded as $value)
                {
                    $data_other_photos = array(
                        'c_id' => $ci_d,
                        'image' => $value
                    );

                    $this->db->insert('community_photos', $data_other_photos);
                }
            }
        }

        echo $ci_d;
    }

    public function communities()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gated :: Communities',
            's_id' => $session,
            'page' => $this->uri->segment(2),
            'communities' => $this->Admin_model->get_admin_communities($session)
        );

        $this->load->view('admin/temps/head', $data);
        $this->load->view('admin/temps/header', $data);
        $this->load->view('admin/temps/sidebar', $data);
        $this->load->view('admin/pages/communities', $data);
        $this->load->view('admin/pages/footer', $data);
        $this->load->view('admin/temps/scripts', $data);
    }

    public function community()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gated :: Manage Community',
            's_id' => $session,
            'page' => $this->uri->segment(2),
            'c_id' => $this->uri->segment(3)
        );

        $this->load->view('admin/temps/head', $data);
        $this->load->view('admin/temps/header', $data);
        $this->load->view('admin/temps/sidebar', $data);
        $this->load->view('admin/pages/community', $data);
        $this->load->view('admin/pages/footer', $data);
        $this->load->view('admin/temps/scripts', $data);
    }

    public function manage()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gated :: Manage Community',
            's_id' => $session,
            'page' => $this->uri->segment(2),
            'c_id' => $this->uri->segment(3),
            'c_details' => $this->Admin_model->get_community_details($this->uri->segment(3))
        );

        $this->load->view('admin/temps/head', $data);
        $this->load->view('admin/temps/header', $data);
        $this->load->view('admin/temps/sidebar', $data);
        $this->load->view('admin/pages/manage', $data);
        $this->load->view('admin/pages/footer', $data);
        $this->load->view('admin/temps/scripts', $data);
    }

    public function edit_community_basic()
    {
        $c_id = $this->input->post('c_id');

        $basic_data = array(
            'name' => $this->input->post('c_name'),
            'city' => $this->input->post('city'),
            'location' => $this->input->post('location'),
            'website' => $this->input->post('website'),
        );

        $this->db->where('c_id', $c_id);
        $this->db->update('community', $basic_data);
    }

    public function edit_community_contact()
    {
        $c_id = $this->input->post('c_id');

        $basic_data = array(
            'p_fname' => $this->input->post('fname'),
            'p_lname' => $this->input->post('lname'),
            'p_email' => $this->input->post('email'),
            'p_phone' => $this->input->post('phone'),
        );

        $this->db->where('c_id', $c_id);
        $this->db->update('community', $basic_data);
    }

    public function edit_community_description()
    {
        $c_id = $this->input->post('c_id');

        $basic_data = array(
            'description' => $this->input->post('description'),
        );

        $this->db->where('c_id', $c_id);
        $this->db->update('community', $basic_data);
    }


    public function delete_community()
    {
        $c_id = $this->uri->segment(3);

        $this->db->where('c_id', $c_id);
        $this->db->set('deleted', 1);
        $this->db->update('community');
    }


    public function edit_features()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gated :: Manage Community',
            's_id' => $session,
            'page' => $this->uri->segment(2),
            'c_id' => $this->uri->segment(3),
            'c_details' => $this->Admin_model->get_community_details($this->uri->segment(3))
        );

        $this->load->view('admin/temps/head', $data);
        $this->load->view('admin/temps/header', $data);
        $this->load->view('admin/temps/sidebar', $data);
        $this->load->view('admin/pages/edit_features', $data);
        $this->load->view('admin/pages/footer', $data);
        $this->load->view('admin/temps/scripts', $data);
    }

    public function update_community_features()
    {
        $c_id = $this->input->post('c_id');
        $amenities = $this->input->post('amenities');

        $this->db->where('c_id', $c_id);
        $this->db->delete('amenities');

        foreach ($amenities as $amenity)
        {
            $data = array(
                'c_id' => $c_id,
                'amenity' => $amenity
            );

            $this->db->insert('amenities', $data);
        }
    }

    public function courts()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gated :: Community Courts',
            's_id' => $session,
            'page' => $this->uri->segment(2),
            'c_id' => $this->uri->segment(3),
            'c_details' => $this->Admin_model->get_community_details($this->uri->segment(3))
        );

        $this->load->view('admin/temps/head', $data);
        $this->load->view('admin/temps/header', $data);
        $this->load->view('admin/temps/sidebar', $data);
        $this->load->view('admin/pages/courts', $data);
        $this->load->view('admin/pages/footer', $data);
        $this->load->view('admin/temps/scripts', $data);
    }

    public function delete_court()
    {
        $id = $this->uri->segment(3);

        $this->db->where('id', $id);
        $this->db->set('active', 0);
        $this->db->update('courts');
    }

    public function add_court_details()
    {
        $session = $this->check_session();

        $court_id = uniqid();
        $user_id = uniqid();
        $password = $this->random_num(8);

        $c_name = $this->input->post('c_name');
        $m_fname = $this->input->post('m_fname');
        $m_lname = $this->input->post('m_lname');
        $m_phone = $this->input->post('m_phone');
        $m_email = $this->input->post('m_email');

        $data = array(
            'c_id' => $this->input->post('c_id'),
            'court_id' => $court_id,
            'user_id' => $user_id,
            'name' => $c_name,
            'm_fname' => $m_fname,
            'm_lname' => $m_lname,
            'm_phone' => $m_phone,
            'm_email' => $m_email,
            'active' => 1,
            'added_by' => $session,
            'image' => './assets/images/profile.png'
        );

        $this->db->insert('courts', $data);

        $login_data = array(
            'user_id' => $user_id,
            'email' => $m_email,
            'password' => sha1($password),
            'level' => 2
        );

        $this->db->insert('logins', $login_data);

        $link = base_url()."auth/login";

        $msg = "Hello ".$m_fname."<br />".
                "You have been added as "."<b>".$c_name."</b> Manager. <br />".
                "Login Credentials are as follows <br /><br />".
                "Email : ".$m_email."<br />".
                "Password : ".$password."<br />".
                "Login Link : "."<a href='.$link.'>$link</a>";

        $subject = "New Court Created";

        $to = $m_email;

        $this->General->email($msg, $subject, $to);

    }




    function random_num($size)
    {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }

    public function check_session()
    {
        $session = $this->session->userdata('admin_session');

        if ($session)
        {
            return $session['user_id'];
        }
        else
        {
            redirect('auth/login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('admin_session');

        redirect('auth/login');
    }
}