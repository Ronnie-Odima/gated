<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function index()
    {
        $data = array
        (
            'title' => 'Gated :: Login'
        );

        $this->load->view('web/temps/head', $data);
        $this->load->view('web/temps/header', $data);
        $this->load->view('web/temps/sidebar', $data);
        $this->load->view('web/pages/body', $data);
        $this->load->view('web/pages/footer', $data);
        $this->load->view('web/temps/scripts', $data);
    }

    public function signup()
    {
        $data = array
        (
            'title' => 'Gated :: SignUp'
        );

        $this->load->view('auth/temps/head', $data);
        $this->load->view('auth/pages/reg', $data);
        $this->load->view('auth/temps/scripts', $data);
    }

    public function login()
    {
        $data = array
        (
            'title' => 'Gated :: Login'
        );

        $this->load->view('auth/temps/head', $data);
        $this->load->view('auth/pages/login', $data);
        $this->load->view('auth/temps/scripts', $data);
    }

    public function reg_action()
    {
        $a_id = uniqid();

        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $image = "./assets/image/profile.png";
        $password = $this->input->post('password');

        $data = array(
            'fname' => $fname,
            'lname' => $lname,
            'email' => $email,
            'phone' => $phone,
            'a_id' => $a_id,
            'image' => $image
        );

        $email_exists = $this->db->get_where('admin', array('email' => $email))->num_rows();

        if($email_exists > 0)
        {
            echo "1";
        }
        else
        {
            $this->db->insert('admin', $data);

            $login_data = array(
                  'user_id' => $a_id,
                  'email' => $email,
                  'password' => sha1($password),
                  'level' => 1,
            );

            $this->db->insert('logins', $login_data);

            echo $a_id;
        }
    }

    public function login_action()
    {
        $email = $this->input->post('email');
        $password = sha1($this->input->post('password'));

        $login = $this->db->get_where('logins', array('email' => $email, 'password' => $password));

        if($login->num_rows() == 1)
        {
            echo $login->row('user_id');
        }
        else
        {
            echo "1";
        }
    }

    public function login_ajax()
    {
        $a_id = $this->uri->segment(3);
        $login = $this->db->get_where('logins', array('user_id' => $a_id));

        if($login->num_rows() == 1)
        {

            if($login->row('level') == 1)
            {
                $sess_data = array(
                    'user_id' => $a_id
                );

                $this->session->set_userdata('admin_session', $sess_data);

                redirect('admin/dashboard');
            }

            if($login->row('level') == 2)
            {
                $sess_data = array(
                    'user_id' => $a_id
                );

                $this->session->set_userdata('court_session', $sess_data);

                redirect('court/dashboard');
            }

        }
    }

    public function check_email()
    {
        $email = $this->input->post('email');

        $exists = $this->db->get_where('logins', array('email' => $email));

        if ($exists->num_rows() == 1)
        {
            $this->db->where('email' , $email);
            $this->db->set('reset_pass', 1);
            $this->db->update('logins');

        }
        else
        {
            echo "1";
        }
    }

    public function send_password_emails()
    {
        $emails = $this->db->get_where('logins', array('reset_pass' => 1))->result();

        foreach ($emails as $email)
        {
            $details = $this->db->get_where('admin', array('a_id' => $email->user_id));

            $fname = $details->row('fname');
            $lname = $details->row('lname');
            $name = $fname." ".$lname;
            $link = base_url()."auth/login";

            $raw_pass = $this->random_num(8);
            $hash_pass = sha1($raw_pass);

            $message = $this->email_message($email->email, $raw_pass, $name, $link);
            $subject = "Password Recovery";
            $to = $email->email;

            $this->db->where('email', $email->email);
            $this->db->set('password', $hash_pass);
            $this->db->set('reset_pass', 0);
            $this->db->update('logins');

            $this->General->email($message, $subject, $to);
        }
    }


    function random_num($size)
    {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }

    public function email_message($email, $password, $name, $link)
    {

        $msg = "Hello ".$name." ,"."<br /><br />".
            "Email Address : ".$email."<br />".
            "Password : ".$password."<br />".
            "Login Link : ". $link;

        return $msg;
    }

}