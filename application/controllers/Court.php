<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Court extends CI_Controller
{
    public function dashboard()
    {
        $session = $this->check_session();

        /*$data = array
        (
            'title' => 'Home',
            's_id' => $session,
            'page' => $this->uri->segment(2)
        );

        $this->load->view('court/temps/head', $data);
        $this->load->view('court/temps/header', $data);
        $this->load->view('court/temps/sidebar', $data);
        $this->load->view('court/pages/dashboard', $data);
        $this->load->view('court/pages/footer', $data);
        $this->load->view('court/temps/scripts', $data);*/

        $this->add_property();
    }

    public function property()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gated :: Manage Property',
            's_id' => $session,
            'page' => $this->uri->segment(2),
            'b_id' => $this->uri->segment(3),
            'c_details' => $this->Court_model->get_court_details($session)
        );

        $this->load->view('court/temps/head', $data);
        $this->load->view('court/temps/header', $data);
        $this->load->view('court/temps/sidebar', $data);
        $this->load->view('court/pages/property', $data);
        $this->load->view('court/pages/footer', $data);
        $this->load->view('court/temps/scripts', $data);
    }

    public function properties()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gated :: Manage Properties',
            's_id' => $session,
            'page' => $this->uri->segment(2),
            'c_details' => $this->Court_model->get_court_details($session)
        );

        $this->load->view('court/temps/head', $data);
        $this->load->view('court/temps/header', $data);
        $this->load->view('court/temps/sidebar', $data);
        $this->load->view('court/pages/properties', $data);
        $this->load->view('court/pages/footer', $data);
        $this->load->view('court/temps/scripts', $data);
    }

    public function add_property()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Gted:: Add Property',
            's_id' => $session,
            'page' => $this->uri->segment(2),
            'c_details' => $this->Court_model->get_court_details($session)
        );

        $this->load->view('court/temps/head', $data);
        $this->load->view('court/temps/header', $data);
        $this->load->view('court/temps/sidebar', $data);
        $this->load->view('court/pages/add_property', $data);
        $this->load->view('court/pages/footer', $data);
        $this->load->view('court/temps/scripts', $data);
    }

    public function submit_house_details()
    {
        $c_id = $this->input->post('c_id');
        $court_id = $this->input->post('court_id');
        $u_acc_no = $this->input->post('utility_ac_no');
        $h_type = $this->input->post('type');
        $b_id = uniqid();

        $data_house = array(
            'court_id' => $court_id,
            'c_id' => $c_id,
            'b_id' => $b_id,
            'name' => $this->input->post('h_name'),
            'type' => $h_type,
            'units' => $this->input->post('units'),
            'shared' => $this->input->post('shared_utility_acc_state')
        );

        $this->db->insert('buildings', $data_house);

        if($u_acc_no)
        {
            $data_utility  = array(
                'b_id' => $b_id,
                'utility_account' => $u_acc_no
            );

            $this->db->insert('utilty_building', $data_utility);
        }

        if($h_type == 'Apartment')
        {
            echo $b_id;
        }
    }

    public function add_units()
    {
        $b_id = $this->input->post('b_id');
        $units = $this->input->post('unit_name');

        foreach ($units as $unit)
        {
            $u_id = uniqid();

            $data = array(
                'name' => $unit,
                'b_id' => $b_id,
                'u_id' => $u_id
            );

            $this->db->insert('building_units', $data);
        }
    }

    public function add_units_csv()
    {
        $b_id = $this->input->post('b_id');

        //validate whether uploaded file is a csv file
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes))
        {
            if (is_uploaded_file($_FILES['file']['tmp_name']))
            {
                //open uploaded csv file with read only mode
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');

                //skip first lines
                fgetcsv($csvFile);

                //parse data from csv file line by line
                while (($line = fgetcsv($csvFile)) !== FALSE)
                {
                    $prevResult = true;

                    if ($prevResult !== false)
                    {
                        $u_id = uniqid();

                        $data = array(
                            'name' => $line[0],
                            'b_id' => $b_id,
                            'u_id' => $u_id
                        );

                        $this->db->insert('building_units', $data);
                    }
                }

                fclose($csvFile);

                echo "success";

                exit(0);


            } else {
                echo "0";

                exit(0);
            }
        }
        else
        {
            echo "1";

            exit(0);
        }
    }


    function random_num($size)
    {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }

    public function check_session()
    {
        $session = $this->session->userdata('court_session');

        if ($session)
        {
            return $session['user_id'];
        }
        else
        {
            redirect('auth/login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('court_session');

        redirect('auth/login');
    }
}