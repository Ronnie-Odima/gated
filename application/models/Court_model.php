<?php
/**
 * Created by PhpStorm.
 * User: ronnie-odima
 * Date: 5/1/18
 * Time: 4:54 PM
 */

class Court_model extends CI_Model
{
    public function get_court_details($id)
    {
        $this->db->where('user_id', $id);
        $result = $this->db->get('courts');

        return $result;
    }

}