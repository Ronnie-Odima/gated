<?php


class Admin_model extends CI_Model
{
    public function get_admin_communities($id)
    {
        $data = $this->db->get_where('community', array('added_by' => $id, 'deleted' => '0'));

        if($data->num_rows() > 0)
        {
            return $data->result();
        }
        else
        {
            return false;
        }
    }

    public function get_community_properties($id)
    {
        $data = $this->db->get_where('buildings', array('c_id' => $id));

        return $data;
    }

    public function get_community_details($id)
    {
        $data = $this->db->get_where('community', array('c_id' => $id));

        return $data;
    }

    public function get_admin_details($id)
    {
        $data = $this->db->get_where('admin', array('a_id' => $id));

        return $data;
    }
}