<?php foreach ($c_details->result() as $c_detail) {?>
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 align-self-center">
                <h3 class="text-themecolor">
                    <b>
                        <?php
                        echo ucwords($c_detail->name, " ");
                        ?>
                    </b>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5>
                            Add A Property To This Court
                        </h5>
                        <hr>
                        <form action="<?php echo base_url('court/submit_house_details')?>" method="post" id="house_details_form">
                            <input type="hidden" name="c_id" value="<?php echo $c_detail->c_id ;?>">
                            <input type="hidden" name="court_id" value="<?php echo $c_detail->court_id ;?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="h_name">House Name :</label>
                                        <input type="text" class="form-control" id="h_name" name="h_name" required> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="house_type">House Type :</label>
                                        <select class="selectpicker" data-style="form-control btn-secondary" id="house_type" name="type" onchange="check_house_status()">
                                            <option value=""> --- Add Type ---</option>
                                            <option value="Apartment">Apartment</option>
                                            <option value="Single">Single House</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="units_no" style="display: none;">
                                        <label for="units">Number of Units :</label>
                                        <input type="text" class="form-control" id="units" name="units">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="shared_utility_acc" style="display: none;">
                                        <label for="shared_utility_acc_state">Shared Utility Account :</label>
                                        <select class="selectpicker" data-style="form-control btn-secondary" id="shared_utility_acc_state" name="shared_utility_acc_state" onchange="check_shared_utility_state()">
                                            <option value=""> --- Add Option ---</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="utility_ac_no" style="display: none;">
                                        <label for="utility_ac_no">Utility Account No. :</label>
                                        <input type="text" class="form-control" id="utility_ac_no" name="utility_ac_no">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button class="btn btn-block btn-dark" type="submit" style="width: auto">
                                            Submit House  Details
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php }?>
