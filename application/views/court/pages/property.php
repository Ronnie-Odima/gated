<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                <?php
                $property_name = $this->db->get_where('buildings', array('b_id' => $b_id))->row('name');

                echo ucwords($property_name, " ");

                ?>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5>
                        Add A Units To This Apartment
                    </h5>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-sm-3">
                            <h6>Enter Details Below</h6>
                            <hr>
                            <form method="post" action="<?php echo base_url('court/add_units')?>" id="unit_add_form">
                                <input type="hidden" name="b_id" value="<?php echo $b_id?>">
                                <div id="education_fields"></div>
                                <div class="row">
                                    <div class="col-sm-12 nopadding">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="unit_name" name="unit_name[]" placeholder="Unit name">
                                                <div class="input-group-append">
                                                    <button class="btn btn-success" type="button" onclick="education_fields();"><i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <hr>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-block btn-dark" style="width: auto;">
                                            Submit Units
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8 col-sm-9">
                            <h6>Upload CSV</h6>
                            <hr>
                            <form method="post" action="<?php echo base_url('court/add_units_csv')?>" id="unit_add_form_csv" enctype="multipart/form-data">
                                <input type="hidden" name="b_id" value="<?php echo $b_id?>">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="file" name="file" id="input-file-max-fs" class="dropify" data- data-max-file-size="2M" required />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <hr>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-block btn-dark" style="width: auto;">
                                            Submit Units CSV
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
