</div>
</div>


<script src="<?php echo base_url()?>assets/node_modules/jquery/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/waves.js"></script>
<script src="<?php echo base_url()?>assets/js/sidebarmenu.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/moment/min/moment.min.js"></script>

<?php if($page == 'add_property') {?>
    <script src="<?php echo base_url()?>assets/node_modules/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/node_modules/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script>
        $(function() {
            $(".select2").select2();
            $('.selectpicker').selectpicker();
        });
    </script>
    <script>
        function check_house_status() {
            var type = $("#house_type").val();

            if(type === 'Apartment')
            {
                $("#utility_ac_no").hide();
                $("#units_no").show();
                $("#shared_utility_acc").show();
            }

            if(type === 'Single')
            {
                $("#units_no").hide();
                $("#shared_utility_acc").hide();
                $("#utility_ac_no").show();
            }

        }
    </script>
    <script>
        function check_shared_utility_state() {
            var state = $("#shared_utility_acc_state").val();

            if(state === '1')
            {
                $("#utility_ac_no").show();
            }

            if(state === '0')
            {
                $("#utility_ac_no").hide();
            }

        }
    </script>
<?php }?>

<?php if($page == 'property') {?>
    <script src="<?php echo base_url()?>assets/node_modules/dff/dff.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script>
        $(function() {
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
<?php }?>

<script src="<?php echo base_url()?>assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
<script src="<?php echo base_url() ;?>assets/external/custom.js"></script>
</body>

</html>