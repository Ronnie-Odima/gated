

<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">--- COURT PROPERTIES PANEL  ---</li>
                <li class="<?php echo ($page == 'add_property' ? 'active' : '')?>"> <a class="waves-effect waves-dark" href="<?php echo base_url('court/add_property')?>" aria-expanded="false"><i class="icon-Car-Wheel"></i>
                        <span class="hide-menu">
                            Add Property
                        </span>
                    </a>
                </li>
                <li class="<?php echo ($page == 'properties' ? 'active' : '')?>">
                    <a class="waves-effect waves-dark" href="<?php echo base_url('court/properties')?>" aria-expanded="false"><i class="icon-Double-Circle"></i>
                        <span class="hide-menu">
                            All Properties
                        </span>
                    </a>
                </li>
                <li class="nav-small-cap">--- COURT RESIDENTS PANEL  ---</li>
                <li class="<?php echo ($page == 'add_resident' ? 'active' : '')?>"> <a class="waves-effect waves-dark" href="<?php echo base_url('court/add_resident')?>" aria-expanded="false"><i class="icon-Car-Wheel"></i>
                        <span class="hide-menu">
                            Add Residents
                        </span>
                    </a>
                </li>
                <li class="<?php echo ($page == 'residents' ? 'active' : '')?>">
                    <a class="waves-effect waves-dark" href="<?php echo base_url('court/residents')?>" aria-expanded="false"><i class="icon-Double-Circle"></i>
                        <span class="hide-menu">
                            All Residents
                        </span>
                    </a>
                </li>
                <!--<li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="icon-Box-Full"></i>
                        <span class="hide-menu">Courts</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="app-email.html">Mailbox</a></li>
                        <li><a href="app-email-detail.html">Mailbox Detail</a></li>
                        <li><a href="app-compose.html">Compose Mail</a></li>
                    </ul>
                </li>-->
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>


<div class="page-wrapper">