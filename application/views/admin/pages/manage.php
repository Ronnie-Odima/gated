<?php foreach ($c_details->result() as $value) {?>
    <?php $admin_details = $this->Admin_model->get_admin_details($value->added_by);?>
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-8 align-self-center">
                <h3 class="text-themecolor">
                    <?php

                    $community_name = $c_details->row()->name;

                    echo ucwords(strtolower($community_name), " ");
                    ?>
                </h3>
            </div>
            <div class="col-md-4 pull-right">
                Added by
                &nbsp;
               <strong>
                   <?php echo ucfirst($admin_details->row()->fname)." ".ucfirst($admin_details->row()->lname)?>
               </strong>
                &nbsp;
                On
                &nbsp;
                <b>
                    <?php echo date_format(date_create($value->date), 'jS M, Y');?>
                </b>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            Basic Information
                        </div>
                        <hr>
                        <table class="table table-bordered table-striped ">
                            <tr>
                                <th style="width: 30%;">
                                    Name
                                </th>
                                <td>
                                    <i class="fa fa-bookmark"></i>
                                    &nbsp;
                                    <?php echo ucwords(strtolower($community_name), " ");?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    City
                                </th>
                                <td>
                                    <i class="fa fa-map-marker"></i>
                                    &nbsp;
                                    <?php echo ucwords(strtolower($value->city), " ");?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Location
                                </th>
                                <td>
                                    <i class="fa fa-map-marker"></i>
                                    &nbsp;
                                    <?php echo ucwords(strtolower($value->location), " ");?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Website
                                </th>
                                <td>
                                    <i class="fa fa-link"></i>
                                    &nbsp;
                                    <?php echo ($value->website == '' ? '' : $value->website);?>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#responsive-modal">
                            Edit Basic Details
                        </button>
                        <form id="basic_details_form<?php echo $value->id ;?>" action="<?php echo base_url('admin/edit_community_basic')?>" method="post">
                            <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Edit Basic Information</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="c_id" value="<?php echo $value->c_id ;?>">
                                            <div class="form-group">
                                                <label for="c_name">Name</label>
                                                <input type="text" class="form-control" id="c_name" name="c_name" value="<?php echo $value->name?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <input type="text" class="form-control" id="city" name="city" value="<?php echo $value->city?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="location">Location</label>
                                                <input type="text" class="form-control" id="location" name="location" value="<?php echo $value->location?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="website">Website</label>
                                                <input type="url" class="form-control" id="website" name="website" value="<?php echo $value->website?>" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success waves-effect waves-light">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            Contact Person
                        </div>
                        <hr>
                        <table class="table table-bordered table-striped ">
                            <tr>
                                <th style="width: 30%;">
                                   First Name
                                </th>
                                <td>
                                    <i class="fa fa-user"></i>
                                    &nbsp;
                                    <?php echo ucfirst($value->p_fname);?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Last Name
                                </th>
                                <td>
                                    <i class="fa fa-user"></i>
                                    &nbsp;
                                    <?php echo ucfirst($value->p_lname);?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Email Address
                                </th>
                                <td>
                                    <i class="fa fa-envelope"></i>
                                    &nbsp;
                                    <?php echo $value->p_email;?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Phone Number
                                </th>
                                <td>
                                    <i class="fa fa-phone"></i>
                                    &nbsp;
                                    <?php echo $value->p_phone;?>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#contact-info-form">
                            Edit Contact Details
                        </button>
                        <form id="contact_details_form<?php echo $value->id ;?>" action="<?php echo base_url('admin/edit_community_contact')?>" method="post">
                            <div id="contact-info-form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Edit Contact Information</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="c_id" value="<?php echo $value->c_id ;?>">
                                            <div class="form-group">
                                                <label for="f_name">First Name</label>
                                                <input type="text" class="form-control" id="f_name" name="fname" value="<?php echo $value->p_fname?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="lname">Last Name</label>
                                                <input type="text" class="form-control" id="lname" name="lname" value="<?php echo $value->p_lname?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="email_add">Email Address</label>
                                                <input type="email" class="form-control" id="email_add" name="email" value="<?php echo $value->p_email?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone_no">Phone Number</label>
                                                <input type="number" class="form-control" id="phone_no" name="phone" value="<?php echo $value->p_phone?>" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success waves-effect waves-light">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            Community Description
                        </div>
                        <hr>
                        <div style="height: 300px; overflow: auto;">
                            <table class="table table-bordered table-striped ">
                                <tr>
                                    <td style="width: 100%;">
                                        <?php echo ucwords(strtolower($value->description), ".");?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <hr>
                        <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#description-modal">
                            Edit Description
                        </button>
                        <form id="description_details_form<?php echo $value->id ;?>" action="<?php echo base_url('admin/edit_community_description')?>" method="post">
                            <div id="description-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Edit Description</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="c_id" value="<?php echo $value->c_id ;?>">
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <textarea class="summernote form-control" rows="6" name="description" id="description"><?php echo $value->description?></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success waves-effect waves-light">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            Community Features
                        </div>
                        <hr>
                        <div style="height: 300px; overflow: auto;">
                            <?php
                                $features = $this->db->get_where('amenities', array('c_id' => $value->c_id))->result();
                            ?>
                            <table class="table table-bordered table-striped ">
                                <?php foreach ($features as $feature) {?>
                                    <tr>
                                        <td style="width: 100%;">
                                            <?php echo ucwords(strtolower($feature->amenity), ".");?>
                                        </td>
                                    </tr>
                                <?php }?>
                            </table>
                        </div>
                        <hr>
                        <a class="btn btn-sm btn-outline-primary" href="<?php echo base_url('admin/edit_features/'.$value->c_id)?>">
                            Edit Features
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="<?php echo base_url()?>assets/node_modules/jquery/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#contact_details_form<?php echo $value->id ;?>").submit(function (e) {
                e.preventDefault();

                $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: $(this).serializeArray(),
                    success: function () {
                        swal({
                            title: "Edit Successful",
                            text: "Contact Details Edited Successfully.",
                            type: "success",
                            showConfirmButton: true,
                            confirmButtonColor: "#32CD32",
                            showCancelButton: false
                        },function () {
                            window.location.reload();
                        });
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#basic_details_form<?php echo $value->id ;?>").submit(function (e) {
                e.preventDefault();

                $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: $(this).serializeArray(),
                    success: function () {
                        swal({
                            title: "Edit Successful",
                            text: "Basic Details Edited Successfully.",
                            type: "success",
                            showConfirmButton: true,
                            confirmButtonColor: "#32CD32",
                            showCancelButton: false
                        },function () {
                            window.location.reload();
                        });
                    }
                });
            });
        });

    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#description_details_form<?php echo $value->id ;?>").submit(function (e) {
                e.preventDefault();

                $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: $(this).serializeArray(),
                    success: function () {
                        swal({
                            title: "Edit Successful",
                            text: "Description Edited Successfully.",
                            type: "success",
                            showConfirmButton: true,
                            confirmButtonColor: "#32CD32",
                            showCancelButton: false
                        },function () {
                            window.location.reload();
                        });
                    }
                });
            });
        });

    </script>

<?php }?>
