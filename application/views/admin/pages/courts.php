<?php foreach ($c_details->result() as $value) {?>
<?php $admin_details = $this->Admin_model->get_admin_details($value->added_by);?>
    <?php
        $courts = $this->db->get_where('courts', array('c_id' => $value->c_id, 'active' => 1))->result();
    ?>
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-9 align-self-center">
                <h3 class="text-themecolor">
                    <?php

                    $community_name = $c_details->row()->name;

                    echo ucwords(strtolower($community_name), " ");
                    ?>
                </h3>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-dark pull-right" data-toggle="modal" data-target="#courtModal">
                    <i class="fa fa-plus-circle"></i>
                    Add A Court
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form id="courts_form<?php echo $value->id ;?>" action="<?php echo base_url('admin/add_court_details')?>" method="post">
                    <div id="courtModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Court Information</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" name="c_id" value="<?php echo $value->c_id ;?>">
                                    <div class="form-group">
                                        <label for="c_name">Court Name</label>
                                        <input type="text" class="form-control" id="c_name" name="c_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="m_fname">Manager's First Name</label>
                                        <input type="text" class="form-control" id="m_fname" name="m_fname" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="m_lname">Manager's Last Name</label>
                                        <input type="text" class="form-control" id="m_lname" name="m_lname"  required>
                                    </div>
                                    <div class="form-group">
                                        <label for="m_phone">Manager's Phone</label>
                                        <input type="number" class="form-control" id="m_phone" name="m_phone" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="m_email">Manager's Email</label>
                                        <input type="email" class="form-control" id="m_email" name="m_email" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-outline-dark waves-effect waves-light">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            Add Courts To This Community
                        </h4>
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-bookmark"></i> Name</th>
                                    <th><i class="fa fa-user"></i> Manager Name</th>
                                    <th><i class="fa fa-phone"></i> Manager Phone</th>
                                    <th><i class="fa fa-envelope"></i> Manager Email</th>
                                    <th><i class="fa fa-calendar"></i> Date Added</th>
                                    <th><i class="fa fa-cogs"></i> Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($courts as $court) {?>
                                    <tr>
                                        <td>
                                            <?php echo ucwords(strtolower($court->name), " ") ;?>
                                        </td>
                                        <td>
                                            <?php echo ucwords(strtolower($court->m_fname." ".$court->m_lname), " ") ;?>
                                        </td>
                                        <td>
                                            <?php echo $court->m_phone ;?>
                                        </td>
                                        <td>
                                            <?php echo $court->m_email ;?>
                                        </td>
                                        <td>
                                            <?php echo date_format(date_create($court->date), 'jS, M Y')?>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a class="btn btn-sm btn-block btn-outline-dark" href="#" style="width: auto;">
                                                        Manage
                                                    </a>
                                                </li>
                                                <li>
                                                    <button class="btn btn-sm btn-block btn-outline-danger" style="width: auto;" id="delete_court<?php echo $court->id?>">
                                                        Delete
                                                    </button>
                                                </li>
                                            </ul>
                                            <script src="<?php echo base_url()?>assets/node_modules/jquery/jquery.min.js"></script>
                                            <script type="text/javascript">
                                                $("#delete_court<?php echo $court->id?>").click(function () {

                                                    swal({
                                                        title: "Confirm Delete",
                                                        text: "Are you sure you want to delete this Court",
                                                        type: "warning",
                                                        showConfirmButton: true,
                                                        showCancelButton: true
                                                    },function () {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo base_url('admin/delete_court/'.$court->id)?>",
                                                            success: function () {
                                                                swal({
                                                                    title: "Delete Success",
                                                                    text: "Court Deleted Successfully",
                                                                    type: "success",
                                                                    showConfirmButton: true,
                                                                    showCancelButton: false
                                                                },function () {
                                                                    window.location.reload();
                                                                });
                                                            }

                                                        });
                                                    });


                                                });
                                            </script>
                                        </td>
                                    </tr>
                                <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th><i class="fa fa-bookmark"></i> Name</th>
                                    <th><i class="fa fa-user"></i> Manager Name</th>
                                    <th><i class="fa fa-phone"></i> Manager Phone</th>
                                    <th><i class="fa fa-envelope"></i> Manager Email</th>
                                    <th><i class="fa fa-calendar"></i> Date Added</th>
                                    <th><i class="fa fa-cogs"></i> Action</th>
                                </tr>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url()?>assets/node_modules/jquery/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#courts_form<?php echo $value->id ;?>").submit(function (e)
            {
                e.preventDefault();

                $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: $(this).serializeArray(),
                    beforeSend: function () {
                        swal({
                            title: "Process Data",
                            text: "Processing Courts Details...",
                            type: "warning",
                            showConfirmButton: false,
                            showCancelButton: false
                        });
                    },
                    success: function () {
                        swal({
                            title: "Process Successful",
                            text: "Courts Details Added Successfully.",
                            type: "success",
                            showConfirmButton: true,
                            confirmButtonColor: "#32CD32",
                            showCancelButton: false
                        },function () {
                            window.location.reload();
                        });
                    }
                });


            });
        });
    </script>

<?php }?>

