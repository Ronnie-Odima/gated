
<?php foreach ($c_details->result() as $value) {?>
<?php $admin_details = $this->Admin_model->get_admin_details($value->added_by);?>
    <?php
        $amenities = array();
        $features = $this->db->get_where('amenities', array('c_id' => $value->c_id))->result();

        foreach ($features as $feature)
        {
            array_push($amenities, $feature->amenity);
        }
    ?>
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">
                    Edit <?php

                    $community_name = $c_details->row()->name;

                    echo ucwords(strtolower($community_name), " ");
                    ?> Features
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('admin/update_community_features')?>" id="edit_features_form<?php echo $value->id ;?>">
                            <input type="hidden" name="c_id" value="<?php echo $value->c_id?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">
                                        <b>
                                            Indoor Features
                                        </b>
                                    </h4>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Alarm System"  name="amenities[]" <?php echo (in_array("Alarm System", $amenities) ? 'checked' : '')?>> Alarm System
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Dish Washer" name="amenities[]" <?php echo (in_array("Dish Washer", $amenities) ? 'checked' : '')?>> Dish Washer
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Gym" name="amenities[]" <?php echo (in_array("Gym", $amenities) ? 'checked' : '')?> > Gym
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Pay Tv Access"  name="pay_tv_access" <?php echo (in_array("Pay Tv Access", $amenities) ? 'checked' : '')?> > Pay TV access
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ducted Cooling" name="amenities[]" <?php echo (in_array("Ducted Cooling", $amenities) ? 'checked' : '')?> >  Ducted cooling
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Air Conditioning" name="amenities[]" <?php echo (in_array("Air Conditioning", $amenities) ? 'checked' : '')?> > Air conditioning
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Lift" name="amenities[]" <?php echo (in_array("Lift", $amenities) ? 'checked' : '')?> > Lift
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Intercom" name="amenities[]" <?php echo (in_array("Intercom", $amenities) ? 'checked' : '')?> > Intercom
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Built-in Wardrobes" name="amenities[]" id="ward_drobes" <?php echo (in_array("Built-in Wardrobes", $amenities) ? 'checked' : '')?> > Built-in wardrobes
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="FloorBoards" name="floorboards" <?php echo (in_array("FloorBoards", $amenities) ? 'checked' : '')?> > Floorboards
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Open Fire Place" name="amenities[]" <?php echo (in_array("Open Fire Place", $amenities) ? 'checked' : '')?> > Open fireplace
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Split System Heating" name="amenities[]" <?php echo (in_array("Split System Heating", $amenities) ? 'checked' : '')?> > Split-system heating
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Gas Heating" name="amenities[]" <?php echo (in_array("Gas Heating", $amenities) ? 'checked' : '')?> > Gas heating
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ensuite" name="amenities[]" <?php echo (in_array("Ensuite", $amenities) ? 'checked' : '')?> > Ensuite
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ducted Vacuum System" name="amenities[]" <?php echo (in_array("Ducted Vacuum System", $amenities) ? 'checked' : '')?> > Ducted vacuum system
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Broadband Internet Available" name="amenities[]" <?php echo (in_array("Broadband Internet Available", $amenities) ? 'checked' : '')?> > Broadband internet available
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ducted Heating"  name="amenities[]" <?php echo (in_array("Ducted Heating", $amenities) ? 'checked' : '')?> > Ducted heating
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Hydronic Heating" name="amenities[]" <?php echo (in_array("Hydronic Heating", $amenities) ? 'checked' : '')?> > Hydronic heating
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <hr>
                                <div class="col-md-12">
                                    <h4 class="text-center">
                                        <b>
                                            Outdoor Features
                                        </b>
                                    </h4>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="CarPort" name="amenities[]" <?php echo (in_array("CarPort", $amenities) ? 'checked' : '')?> >  Carport
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Remote Garage" name="amenities[]" <?php echo (in_array("Remote Garage", $amenities) ? 'checked' : '')?> > Remote garage
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Tennis Court"  name="amenities[]" <?php echo (in_array("Tennis Court", $amenities) ? 'checked' : '')?> > Tennis court
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Fully Fenced" name="amenities[]" <?php echo (in_array("Fully Fenced", $amenities) ? 'checked' : '')?> > Fully fenced
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Garage" name="amenities[]" <?php echo (in_array("Garage", $amenities) ? 'checked' : '')?> > Garage
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Secure Parking" name="amenities[]" <?php echo (in_array("Secure Parking", $amenities) ? 'checked' : '')?> > Secure parking
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Balcony"  name="amenities[]" <?php echo (in_array("Balcony", $amenities) ? 'checked' : '')?> >  Balcony
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Car Space"  name="amenities[]" <?php echo (in_array("Car Space", $amenities) ? 'checked' : '')?> > Open car spaces
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Swimming Pool" name="amenities[]" <?php echo (in_array("Swimming Pool", $amenities) ? 'checked' : '')?> > Swimming pool
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Deck" name="amenities[]" <?php echo (in_array("Deck", $amenities) ? 'checked' : '')?> > Deck
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <hr>
                                <div class="col-md-12">
                                    <h4 class="text-center">
                                        <b>
                                            Eco Features
                                        </b>
                                    </h4>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Solar Panels" name="amenities[]" <?php echo (in_array("Solar Panels", $amenities) ? 'checked' : '')?> >  Solar panels
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Gray Water system" name="amenities[]" <?php echo (in_array("Gray Water system", $amenities) ? 'checked' : '')?> > Grey water system
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Energy Efficiency Rating - Low"  name="amenities[]" <?php echo (in_array("Energy Efficiency Rating - Low", $amenities) ? 'checked' : '')?> > Energy efficiency rating - Low
                                                        </label>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Solar Hot Water" name="amenities[]" <?php echo (in_array("Solar Hot Water", $amenities) ? 'checked' : '')?> > Solar hot water
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Energy Efficiency Rating - High" name="amenities[]" <?php echo (in_array("Energy Efficiency Rating - High", $amenities) ? 'checked' : '')?> > Energy efficiency rating - High
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Borehole" name="amenities[]" <?php echo (in_array("Borehole", $amenities) ? 'checked' : '')?>> Borehole
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Water Tank"  name="amenities[]" <?php echo (in_array("Water Tank", $amenities) ? 'checked' : '')?> > Water tank
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Efficiency Energy Rating - Medium" name="amenities[]" <?php echo (in_array("Efficiency Energy Rating - Medium", $amenities) ? 'checked' : '')?> > Energy efficiency rating - Medium
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <hr>
                                <div class="col-md-12">
                                    <h4 class="text-center">
                                        <b>
                                            Other Features
                                        </b>
                                    </h4>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Pets Allowed" name="amenities[]" <?php echo (in_array("Pets Allowed", $amenities) ? 'checked' : '')?> >  Pets Allowed
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Water View" name="amenities[]" <?php echo (in_array("Water View", $amenities) ? 'checked' : '')?> > Water View
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Hill/Mountain View"  name="amenities[]"  <?php echo (in_array("Hill/Mountain View", $amenities) ? 'checked' : '')?> > Hill/Mountain View
                                                        </label>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Disability Features" name="amenities[]" <?php echo (in_array("Disability Features", $amenities) ? 'checked' : '')?> > Disability Features
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ocean View"  name="amenities[]" <?php echo (in_array("Ocean View", $amenities) ? 'checked' : '')?> > Ocean View
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Waterfront" name="amenities[]" <?php echo (in_array("Waterfront", $amenities) ? 'checked' : '')?> > Waterfront
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="River View" name="amenities[]" <?php echo (in_array("River View", $amenities) ? 'checked' : '')?> > River View
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <button class="btn btn-dark" type="submit" style="width: auto;">
                                        Edit Features
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url()?>assets/node_modules/jquery/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#edit_features_form<?php echo $value->id ;?>").submit(function (e)
            {
                e.preventDefault();

                $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: $(this).serializeArray(),
                    success: function () {
                        swal({
                            title: "Edit Successful",
                            text: "Community Features Edited Successfully.",
                            type: "success",
                            showConfirmButton: true,
                            confirmButtonColor: "#32CD32",
                            showCancelButton: false
                        },function () {
                            window.location.href = "<?php echo base_url('admin/manage/'.$this->uri->segment(3))?>";
                        });
                    }
                });


            });
        });
    </script>

<?php }?>
