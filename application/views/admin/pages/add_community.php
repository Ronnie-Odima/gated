<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                Add Community
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body wizard-content ">
                    <h4 class="card-title">
                        Community Details
                    </h4>
                    <form action="<?php echo base_url('admin/process_community_data')?>" class="tab-wizard validation-wizard horizontal wizard-circle" enctype="multipart/form-data">
                        <!-- Step 1 -->
                        <h6>Basic Information</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="c_name">Community Name :</label>
                                        <input type="text" class="form-control" id="c_name" name="c_name" required> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">City :</label>
                                        <input type="text" class="form-control" id="city" name="city"> </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Specific Location :</label>
                                        <input type="text" class="form-control" id="location" name="location"> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="website">Website ( <i>If Available</i> ):</label>
                                        <input type="url" class="form-control" id="website" name="website"> </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Description :</label>
                                        <textarea class="summernote" name="description" id="description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 2 -->
                        <h6>Amenities</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">
                                        <b>
                                            Indoor Features
                                        </b>
                                    </h4>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Alarm System"  name="amenities[]"> Alarm System
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Dish Washer" name="amenities[]"> Dish Washer
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Gym" name="amenities[]"> Gym
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Pay Tv Access"  name="pay_tv_access"> Pay TV access
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ducted Cooling" name="amenities[]">  Ducted cooling
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Air Conditioning" name="amenities[]"> Air conditioning
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Lift" name="amenities[]"> Lift
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Intercom" name="amenities[]"> Intercom
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Built-in Wardrobes" name="amenities[]" id="ward_drobes"> Built-in wardrobes
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="FloorBoards" name="floorboards"> Floorboards
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Open Fire Place" name="amenities[]"> Open fireplace
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Split System Heating" name="amenities[]"> Split-system heating
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Gas Heating" name="amenities[]"> Gas heating
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ensuite" name="amenities[]"> Ensuite
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ducted Vacuum System" name="amenities[]"> Ducted vacuum system
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Broadband Internet Available" name="amenities[]"> Broadband internet available
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ducted Heating"  name="amenities[]"> Ducted heating
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Hydronic Heating" name="amenities[]"> Hydronic heating
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <hr>
                                <div class="col-md-12">
                                    <h4 class="text-center">
                                        <b>
                                            Outdoor Features
                                        </b>
                                    </h4>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="CarPort" name="amenities[]">  Carport
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Remote Garage" name="amenities[]"> Remote garage
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Tennis Court"  name="amenities[]"> Tennis court
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Fully Fenced" name="amenities[]"> Fully fenced
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Garage" name="amenities[]"> Garage
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Secure Parking" name="amenities[]"> Secure parking
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Balcony"  name="amenities[]">  Balcony
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Car Space"  name="amenities[]"> Open car spaces
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Swimming Pool" name="amenities[]"> Swimming pool
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Deck" name="amenities[]"> Deck
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <hr>
                                <div class="col-md-12">
                                    <h4 class="text-center">
                                        <b>
                                            Eco Features
                                        </b>
                                    </h4>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Solar Panels" name="amenities[]">  Solar panels
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Gray Water system" name="amenities[]"> Grey water system
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Energy Efficiency Rating - Low"  name="amenities[]"> Energy efficiency rating - Low
                                                        </label>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Solar Hot Water" name="amenities[]"> Solar hot water
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Energy Efficiency Rating - High" name="amenities[]"> Energy efficiency rating - High
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Borehole" name="amenities[]"> Borehole
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Water Tank"  name="amenities[]"> Water tank
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Efficiency Energy Rating - Medium" name="amenities[]" > Energy efficiency rating - Medium
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <hr>
                                <div class="col-md-12">
                                    <h4 class="text-center">
                                        <b>
                                            Other Features
                                        </b>
                                    </h4>
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Pets Allowed" name="amenities[]">  Pets Allowed
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Water View" name="amenities[]"> Water View
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Hill/Mountain View"  name="amenities[]"> Hill/Mountain View
                                                        </label>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Disability Features" name="amenities[]"> Disability Features
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Ocean View"  name="amenities[]"> Ocean View
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="Waterfront" name="amenities[]"> Waterfront
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"  value="River View" name="amenities[]"> River View
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 3 -->
                        <h6>Images</h6>
                        <section>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                Profile Image
                                            </h4>
                                            <hr>
                                            <label for="input-file-max-fs">This will be the main image</label>
                                            <input type="file" name="main_image" id="input-file-max-fs" class="dropify" data- data-max-file-size="5M" accept="image/*" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                Other Images
                                            </h4>
                                            <hr>
                                            <label for="input-file-now-custom-2">Can Select Multiple</label>
                                            <input type="file" name="other_images[]" id="input-file-now-custom-2" data-show-remove="false" class="dropify" multiple accept="image/*" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 4 -->
                        <h6>Contact Information</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="p_fname">Contact Person First Name :</label>
                                        <input type="text" class="form-control" id="p_fname" name="p_fname">
                                    </div>
                                    <div class="form-group">
                                        <label for="p_lame">Contact Person Last Name</label>
                                        <input type="text" class="form-control" id="p_lame" name="p_lname">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="p_email">Contact Person Email Address:</label>
                                        <input type="email" class="form-control" id="p_email" name="p_email">
                                    </div>
                                    <div class="form-group">
                                        <label for="p_phone">Contact Person Phone Number</label>
                                        <input type="tel" class="form-control" id="p_phone" name="p_phone">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
