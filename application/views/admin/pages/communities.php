<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">My Communities</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"></h4>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><i class="fa fa-bookmark"></i> Name</th>
                                <th><i class="fa fa-building"></i> No. of Properties</th>
                                <th><i class="fa fa-map-marker"></i> City</th>
                                <th><i class="fa fa-map-marker"></i> Location</th>
                                <th><i class="fa fa-calendar"></i> Date Added</th>
                                <th><i class="fa fa-cogs"></i> Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($communities as $community) {?>
                                <tr>
                                    <td>
                                        <?php echo ucwords(strtolower($community->name), " ") ;?>
                                    </td>
                                    <td>
                                        <?php
                                            $properties_no = $this->Admin_model->get_community_properties($community->c_id);

                                            echo $properties_no->num_rows();
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo ucwords(strtolower($community->city), " ") ;?>
                                    </td>
                                    <td>
                                        <?php echo ucwords(strtolower($community->location), " ")?>
                                    </td>
                                    <td>
                                        <?php echo date_format(date_create($community->date), 'jS, M Y')?>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            <li>
                                                <a class="btn btn-sm btn-block btn-outline-dark" href="<?php echo base_url('admin/manage/'.$community->c_id)?>" style="width: auto;">
                                                    Manage
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn btn-sm btn-block btn-outline-primary" href="<?php echo base_url('admin/courts/'.$community->c_id)?>" style="width: auto;">
                                                    View Courts
                                                </a>
                                            </li>
                                            <li>
                                                <button class="btn btn-sm btn-block btn-outline-danger" style="width: auto;" id="delete_community<?php echo $community->id?>">
                                                    Delete
                                                </button>
                                            </li>
                                        </ul>
                                        <script src="<?php echo base_url()?>assets/node_modules/jquery/jquery.min.js"></script>
                                        <script type="text/javascript">
                                            $("#delete_community<?php echo $community->id?>").click(function () {


                                                swal({
                                                    title: "Confirm Delete",
                                                    text: "Are you sure you want to delete this opportunity",
                                                    type: "warning",
                                                    showConfirmButton: true,
                                                    showCancelButton: true
                                                },function () {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo base_url('admin/delete_community/'.$community->c_id)?>",
                                                        success: function () {
                                                            swal({
                                                                title: "Delete Success",
                                                                text: "Community Deleted Successfully",
                                                                type: "success",
                                                                showConfirmButton: true,
                                                                showCancelButton: false
                                                            },function () {
                                                                window.location.reload();
                                                            });
                                                        }

                                                    });
                                                });


                                            });
                                        </script>
                                    </td>
                                </tr>
                            <?php }?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><i class="fa fa-bookmark"></i> Name</th>
                                <th><i class="fa fa-building"></i> No. of Properties</th>
                                <th><i class="fa fa-map-marker"></i> City</th>
                                <th><i class="fa fa-map-marker"></i> Location</th>
                                <th><i class="fa fa-calendar"></i> Date Added</th>
                                <th><i class="fa fa-cogs"></i> Action</th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
