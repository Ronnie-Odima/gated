

<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">--- COMMUNITY PANEL  ---</li>
                <li class="<?php echo ($page == 'add_community' ? 'active' : '')?>"> <a class="waves-effect waves-dark" href="<?php echo base_url('admin/add_community')?>" aria-expanded="false"><i class="icon-Car-Wheel"></i>
                        <span class="hide-menu">
                            Add Community
                        </span>
                    </a>
                </li>
                <li class="<?php echo ($page == 'communities' ? 'active' : '')?>">
                    <a class="waves-effect waves-dark" href="<?php echo base_url('admin/communities')?>" aria-expanded="false"><i class="icon-Double-Circle"></i>
                        <span class="hide-menu">
                            Communities
                        </span>
                    </a>
                </li>
                <!--<li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="icon-Box-Full"></i>
                        <span class="hide-menu">Courts</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="app-email.html">Mailbox</a></li>
                        <li><a href="app-email-detail.html">Mailbox Detail</a></li>
                        <li><a href="app-compose.html">Compose Mail</a></li>
                    </ul>
                </li>-->
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>


<div class="page-wrapper">