</div>
</div>


<script src="<?php echo base_url()?>assets/node_modules/jquery/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/ps/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/waves.js"></script>
<script src="<?php echo base_url()?>assets/js/sidebarmenu.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/moment/min/moment.min.js"></script>

<?php if($page == 'add_community') {?>
    <script src="<?php echo base_url()?>assets/node_modules/wizard/jquery.steps.min.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/wizard/jquery.validate.min.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/wizard/steps.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/summernote/dist/summernote.min.js"></script>
    <script>
        $(function() {

            $('.summernote').summernote({
                height: 300, // set editor height
                minHeight: 300, // set minimum height of editor
                maxHeight: 300, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });

            $('.inline-editor').summernote({
                airMode: true
            });

        });

        window.edit = function() {
            $(".click2edit").summernote()
        },
            window.save = function() {
                $(".click2edit").summernote('destroy');
            }
    </script>
    <script>
        $(function() {
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
<?php }?>

<?php if($page == 'community') {?>
    <script src="<?php echo base_url()?>assets/node_modules/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/node_modules/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script>
        $(function() {
            $(".select2").select2();
            $('.selectpicker').selectpicker();
        });
    </script>
    <script>
        function check_house_status() {
            var type = $("#house_type").val();

            if(type === 'Apartment')
            {
                $("#utility_ac_no").hide();
                $("#units_no").show();
                $("#shared_utility_acc").show();
            }

            if(type === 'Single')
            {
                $("#units_no").hide();
                $("#shared_utility_acc").hide();
                $("#utility_ac_no").show();
            }

        }
    </script>
    <script>
        function check_shared_utility_state() {
            var state = $("#shared_utility_acc_state").val();

            if(state === '1')
            {
                $("#utility_ac_no").show();
            }

            if(state === '0')
            {
                $("#utility_ac_no").hide();
            }

        }
    </script>
<?php }?>

<?php if($page == 'property') {?>
    <script src="<?php echo base_url()?>assets/node_modules/dff/dff.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script>
        $(function() {
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
<?php }?>

<?php if($page == 'communities') {?>
    <script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/datatables/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $(function() {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });

        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>
<?php }?>

<?php if($page == 'manage') {?>
    <script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/node_modules/summernote/dist/summernote.min.js"></script>
<?php }?>

<?php if($page == 'edit_features') {?>
    <script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
<?php }?>

<?php if($page == 'courts') {?>
<script src="<?php echo base_url()?>assets/node_modules/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/node_modules/datatables/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<script>
    $(function() {
        $('#myTable').DataTable();
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });

    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>
<?php }?>

<script src="<?php echo base_url()?>assets/node_modules/styleswitcher/jQuery.style.switcher.js"></script>
<script src="<?php echo base_url() ;?>assets/external/custom.js"></script>
</body>

</html>