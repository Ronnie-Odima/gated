<section id="wrapper" class="login-register login-sidebar" style="background-image:url('<?php echo base_url() ;?>assets/images/background/login-register.jpg');">
    <div class="login-box card">
        <div class="card-body">
            <a href="javascript:void(0)" class="text-center db" style="margin-top: 5vh;">
                <img src="<?php echo base_url() ;?>assets/images/logo.png" alt="Gated by E-Kodi" class="img-responsive" /><br/>
            </a>
            <form class="form-horizontal form-material" method="post" id="loginform" action="<?php echo base_url('auth/login_action')?>" style="margin-top: 10vh;">
                <div class="alert alert-danger" id="wrong_details" style="display: none;">
                    Incorrect Credentials. Try Again.
                </div>
                <div class="form-group m-t-40">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" name="email" required placeholder="Email Address">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" required placeholder="Password">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-primary pull-left p-t-0">
                            <input id="checkbox-signup" type="checkbox" class="filled-in chk-col-light-blue">
                            <label for="checkbox-signup"> Remember me </label>
                        </div>
                        <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block waves-effect waves-light" type="submit" id="submit-btn">
                            <span class="text-center" id="login_text_default">
                                    Login &nbsp;
                            </span>
                            <span class="row">
                                <span class="col-xs-10" id="login_text" style="display: none;">
                                     &nbsp;
                                </span>
                                <span class="col-xs-2" id="loader_login" style="display: none;">
                                    <img src="<?php echo base_url('assets/images/loader/loader.gif')?>" class="img-responsive" style="width: 30px; height: 30px;">
                                </span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        Don't have an account? <a href="<?php echo base_url('auth/signup')?>" class="text-primary m-l-5"><b>Sign Up</b></a>
                    </div>
                </div>
            </form>
            <form class="form-horizontal" id="recoverform" action="<?php echo base_url('auth/check_email')?>" style="margin-top: 10vh;">
                <div class="alert alert-danger" id="wrong_email" style="display: none;">
                    Email Address Does Not Exist.
                </div>
                <div class="alert alert-success" id="correct_email" style="display: none;">
                    Instruction has been sent your email
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <h3>Recover Password</h3>
                        <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" name="email" required placeholder="Email Address">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg btn-block waves-effect waves-light" type="submit" id="submit-btn">
                            <span class="text-center" id="recover_text_default">
                                    Recover Password &nbsp;
                            </span>
                            <span class="row">
                                <span class="col-xs-10" id="recover_text" style="display: none;">
                                     &nbsp;
                                </span>
                                <span class="col-xs-2" id="loader_recover" style="display: none;">
                                    <img src="<?php echo base_url('assets/images/loader/loader.gif')?>" class="img-responsive" style="width: 30px; height: 30px;">
                                </span>
                            </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
