

<section id="wrapper" class="login-register login-sidebar" style="background-image:url('<?php echo base_url() ;?>assets/images/background/login-register.jpg');">
    <div class="login-box card">
        <div class="card-body">
            <a href="javascript:void(0)" class="text-center db" style="margin-top: 5vh;">
                <img src="<?php echo base_url() ;?>assets/images/logo.png" alt="Gated by E-Kodi" class="img-responsive" /><br/>
            </a>
            <form class="form-horizontal form-material" id="registration_form" method="post" action="<?php echo base_url('auth/reg_action')?>">
                <h3 class="box-title m-t-40 m-b-0">Register Now</h3><small>Create your account and enjoy</small>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="fname" required placeholder="First Name">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="lname" required placeholder="Last Name">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="number" name="phone" required placeholder="Phone Number">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" name="email" id="email" required placeholder="Email">
                        <p id="incorrect_email" style="display: none; margin-top: 7px; color: red;">Email Exists</p>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" id="password1" required placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="cpassword" id="password2" required placeholder="Confirm Password">
                        <p id="incorrect_password" style="display: none; margin-top: 7px; color: red;">Retype correct password</p>
                    </div>
                </div>

                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block waves-effect waves-light" type="submit" id="submit-btn">
                            <span class="text-center" id="button_text_default">
                                    Sign Up &nbsp;
                            </span>
                            <span class="row">
                                <span class="col-xs-10" id="button_text" style="display: none;">
                                     &nbsp;
                                </span>
                                <span class="col-xs-2" id="loader" style="display: none;">
                                    <img src="<?php echo base_url('assets/images/loader/loader.gif')?>" class="img-responsive" style="width: 30px; height: 30px;">
                                </span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Already have an account? <a href="<?php echo base_url('auth/login')?>" class="text-info m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
