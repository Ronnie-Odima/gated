<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ;?>assets/images/favicon.png">
    <title><?php echo $title ;?></title>

    <link href="<?php echo base_url() ;?>assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url() ;?>assets/css/pages/login-register-lock.css" rel="stylesheet">

    <link href="<?php echo base_url() ;?>assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url() ;?>assets/css/colors/default.css" id="theme" rel="stylesheet">

    <link href="<?php echo base_url() ;?>assets/external/custom.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Gated by Ekodi</p>
    </div>
</div>