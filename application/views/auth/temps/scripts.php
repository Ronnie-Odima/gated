<script src="<?php echo base_url() ;?>assets/node_modules/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ;?>assets/node_modules/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url() ;?>assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ;?>assets/external/custom.js"></script>

<script type="text/javascript">
    $(function() {
        $(".preloader").fadeOut();
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
</script>

</body>

</html>